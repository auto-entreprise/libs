<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions utilitaires sur les string
 */
class LibString {

    /**
     * Permet de "caméliser" une chaine de caractères
     * Ex : "string de test" => "stringDeTest" (StringDeTest si pascalCase)
     *
     * @param string $string
     * @param boolean $pascalCase
     * @return string
     */
    public static function camelize($string, $pascalCase = false)
    {
        $string = self::stripAccents($string);
        $string = str_replace(array('-', '_', '(', ')'), ' ', strtolower($string));
        $string = trim($string);
        $string = ucwords($string);
        $string = str_replace(' ', '', $string);

        if (!$pascalCase) {
            return lcfirst($string);
        }
        return $string;
    }

    /**
     * Filter a name to only allow valid variable characters
     *
     * @param  string $value
     * @param  bool $allowBrackets
     * @return string
     */
    public static function zendCamelize($value, $allowBrackets = false)
    {
        $charset = '^a-zA-Z0-9_\x7f-\xff';
        if ($allowBrackets) {
            $charset .= '\[\]';
        }
        return preg_replace('/[' . $charset . ']/', '', (string) $value);
    }

    /**
     * Permet de "décaméliser" une chaine de caractères
     * Ex : "stringDeTest" => "string_de_test"
     *
     * @param string $string
     * @return string
     */
    public static function uncamelize($string){
        $matches = array();
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);

    }

    /**
     * Permet de mettre une chaine de caractères en format "constante"
     * Ex : "string de test (encore)" => "STRING_DE_TEST_ENCORE"
     *
     * @param string $string
     * @return string
     */
    public static function constantize($string){
        $string = self::stripAccents($string);
        $string = str_replace(array('"', '\'',), '', $string);
        $string = str_replace(array(' (', '( ', '(', ')',), ' ', $string);
        $string = trim($string);
        $string = str_replace(array(' ', '-', '(', ')', ',', ';', ':'), '_', $string);

        $string = strtoupper($string);

        return $string;
    }

    /**
     * Permet de supprimer les accents d'une chaine de caracteres
     *
     * @param string $string
     * @return string
     */
    public static function stripAccents($string){
        $str = htmlentities($string, ENT_NOQUOTES, 'utf-8');

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caract�res

        return $str;
    }

    /**
     * Permet "iemiser" le contenu d'une chaine de caractères pour de l'affichage HTML
     * Ex iemizeStr('le 84ème élément', 'ième') => "le 84<sup>ième</sup> élément"
     *
     * @param string $str
     * @param string $strToIemized
     * @return string
     */
    public static function iemizeStr($str, $strToIemized){
        return trim(str_replace($strToIemized, "<sup>$strToIemized</sup>", $str));
    }

    /**
     * Fonction de callback
     * Permet d'encadrer une chaine par des quotes
     *
     * @param string $str
     * @return string
     */
    public static function escape($str){
        return "'$str'";
    }

    /**
     * Fonction de callback
     * Permet de mettre en majuscule et d'encadrer une chaine par des quotes
     *
     * @param string $str
     * @return string
     */
    public static function upperAndEscape($str){
        $str = strtoupper($str);
        return self::escape($str);
    }

    /**
     * Fonction d'utilitaire pour CLI
     * Permet de forcer l'affichage temps r�el d'une chaine de caractères
     *
     * @param string $string
     */
    public static function flush($string){
        echo $string;
        ob_flush();
    }

    /**
     * Fonction permettant de formater un numéro de téléphone selon le caractère
     * $sep fourni en paramètre
     *
     * @param string $tel
     * @param string $sep
     * @return string
     */
    public static function getFormatedTel($tel, $sep = '.') {
        $result = $tel;
        $matches = array();
        if (preg_match('/(.*)(\d{2})(\d{2})(\d{2})(\d{2})$/', $tel, $matches)) {
            $result = $matches[1] . $sep . $matches[2] . $sep . $matches[3] . $sep . $matches[4] . $sep . $matches[5];
        }
        return $result;
    }

    /**
     * Supprime tous les caractères non alpha numérique d'une string
     * @param string $string
     * @return string
     */
    public static function keepAlphaNumeric($string) {
        $stringFormat = preg_replace("/[^a-zA-Z0-9]+/", "", $string);

        return $stringFormat;
    }

    /**
     * Récupération des tags dans une chaine de caractère
     * @param string $string
     * @return array
     */
    public static function getTagsFromString($string, $prefix = '#') {
        $tagsList = array();
        preg_match_all('/((^'.$prefix.'[[:graph:]]*)|([ \r\n]{1}('.$prefix.'[[:graph:]]*)))/i', $string, $tagsList);

        if (isset($tagsList[0]) && !empty($tagsList[0])) {
            return array_filter(array_map('trim', $tagsList[0]));
        } else {
            return array();
        }
    }

    /**
     * Récupération des contacts dans une chaine de caractère
     * @param string $string
     * @return array
     */
    public static function getContactsFromString($string, $prefix = '@') {
        $contactsList = array();
        preg_match_all('/((^'.$prefix.'[[:graph:]]*)|([ \r\n]{1}('.$prefix.'[[:graph:]]*)))/i', $string, $contactsList);

        if (isset($contactsList[0]) && !empty($contactsList[0])) {
            return array_filter(array_map('trim', $contactsList[0]));
        } else {
            return array();
        }
    }

    /**
     * Suppression de toutes les ponctuations
     * @param string $string
     * @param array $ignorePunct Ponctuation à ignorer
     * @return string
     */
    public static function removePunctuation($string, $ignorePunct = array())
    {
        $punctuation = '^\P{P}';
        if(!empty($ignorePunct))
        {
            foreach($ignorePunct as $tmp)
            {
                $punctuation = $punctuation.'\\'.$tmp;
            }
        }
        $string = preg_replace('/['.$punctuation.']/u', '', $string);
        $string = preg_replace('/[\x{3002}\x{ff1f}\x{ff01}\x{002c}\x{3001}\x{ff0c}]+/u', '', $string);

        return $string;
    }

    /**
     * Récupération de toutes les ponctuations
     * @param string $string
     * @return array
     */
    public static function getPunctuation($string)
    {
        $matches = array();
        $string = preg_match_all('/[\x{3002}\x{ff1f}\x{ff01}\x{002c}\x{3001}\x{ff0c}[:punct:]]/u', $string, $matches);

        return $matches[0];
    }

    /**
     * Retourne un mot aléatoire à partir d'une liste de mot séparé par un / par défaut
     * @param string $wordsList
     * @param string $separator
     * @return string
     */
    public static function getRandomWord($wordsList, $separator = '/')
    {
        $array = explode($separator, $wordsList);

        return trim($array[array_rand($array)]);
    }

    /**
     * Split une chaîne de caractère encodée en UTF-8
     * @param string $str
     * @param int $len
     * @return array
     */
    public static function utf8Split($str, $len = 1)
    {
        $arr = array();
        $strLen = mb_strlen($str, 'UTF-8');
        for ($i = 0; $i < $strLen; $i++)
        {
            $arr[] = mb_substr($str, $i, $len, 'UTF-8');
        }
        return $arr;
    }

    /**
     * Mise en forme de la ponctuation sur une phrase en français
     * @param $string
     * @return string
     */
    public static function formatFrenchPunctuation($string)
    {
        $spaceBefore = '(\[{';
        $spaceAfter = ',.)\]}…';
        $spaceBeforeAfter = '!:?;';
        $spaceNotAfterException = ')\]}';
        $patternReplace = array(
            '/\s*(['.$spaceBefore.'])\s*/u',
            '/\s*(['.$spaceAfter.'])\s*/u',
            '/\s*(['.$spaceBeforeAfter.'])\s*/u',
            '/\s*(['.$spaceNotAfterException.'])\s([.])*/u'
        );
        $replace = array(
            ' $1',
            '$1 ',
            ' $1 ',
            '$1$2'
        );
        $string = trim(preg_replace($patternReplace, $replace, $string));

        //Optimisation d'exceptions générées par la regex précédente
        $patternReplace = array(
            '/\. \./u',
            '/\?  \?/u',
            '/\!  \!/u',
            '/\!  \?/u',
            '/\?  \!/u'
        );
        $replace = array(
            '..',
            '??',
            '!!',
            '!?',
            '?!'
        );
        do
        {
            $string = preg_replace($patternReplace, $replace, $string, -1, $count);
        }while($count > 0);

        $string = str_replace('  ', ' ', $string);

        return $string;
    }

    /**
     * Mise en forme de la ponctuation sur une phrase en anglais
     * @param $string
     * @return string
     */
    public static function formatEnglishPunctuation($string)
    {
        $spaceAfter = ',.)\]}…!:?;';
        $spaceNotAfterException = ')\]}';
        $patternReplace = array(
            '/\s*(['.$spaceAfter.'])\s*/u',
            '/\s*(['.$spaceNotAfterException.'])\s([.])*/u'
        );
        $replace = array(
            '$1 ',
            '$1$2'
        );
        $string = trim(preg_replace($patternReplace, $replace, $string));

        //Optimisation d'exceptions générées par la regex précédente
        $patternReplace = array(
            '/\. \./u',
            '/\?  \?/u',
            '/\!  \!/u',
            '/\!  \?/u',
            '/\?  \!/u'
        );
        $replace = array(
            '..',
            '??',
            '!!',
            '!?',
            '?!'
        );
        do
        {
            $string = preg_replace($patternReplace, $replace, $string, -1, $count);
        }while($count > 0);

        return $string;
    }

    /**
     * Mise en forme de la ponctuation sur une phrase en espagnol
     * @param $string
     * @return string
     */
    public static function formatSpanishPunctuation($string)
    {
        $spaceBefore = '¿¡';
        $spaceAfter = ',.)\]}…!:?;';
        $spaceNotAfterException = ')\]}';
        $patternReplace = array(
            '/\s*(['.$spaceBefore.'])\s*/u',
            '/\s*(['.$spaceAfter.'])\s*/u',
            '/\s*(['.$spaceNotAfterException.'])\s([.])*/u'
        );
        $replace = array(
            ' $1',
            '$1 ',
            '$1$2'
        );
        $string = trim(preg_replace($patternReplace, $replace, $string));

        //Optimisation d'exceptions générées par la regex précédente
        $patternReplace = array(
            '/\. \./u',
            '/\?  \?/u',
            '/\!  \!/u',
            '/\!  \?/u',
            '/\?  \!/u'
        );
        $replace = array(
            '..',
            '??',
            '!!',
            '!?',
            '?!'
        );
        do
        {
            $string = preg_replace($patternReplace, $replace, $string, -1, $count);
        }while($count > 0);

        return $string;
    }

    /**
     * Majuscule de la première lettre en unicode
     * @param $str
     * @param string $encoding
     * @param bool $lowerStrEnd
     * @return string
     */
    public static function mb_ucfirst($str, $encoding = 'UTF-8', $lowerStrEnd = false)
    {
        $firstLetter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);

        if ($lowerStrEnd)
        {
            $strEnd = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else
        {
            $strEnd = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }

        $str = $firstLetter . $strEnd;

        return $str;
    }

    /**
     * Mise en minuscule de la première lettre en unicode
     * @param $str
     * @param null $encoding
     */
    public static function mb_lcfirst($str, $encoding = null)
    {
        $encoding = $encoding ?? mb_internal_encoding();

        $first = mb_strtolower(
            mb_substr($str, 0, 1, $encoding),
            $encoding
        );

        return $first . mb_substr($str, 1, null, $encoding);
    }

    /**
     * Remplacement du énième caractère trouvé dans une chaine
     * nth démarre à 0
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @param int $nth
     * @return string mixed
     */
    public static function str_replace_nth(string $search, string $replace, string $subject, int $nth)
    {
        $found = preg_match_all('/'.preg_quote($search).'/iu', $subject, $matches, PREG_OFFSET_CAPTURE);
        if (false !== $found && $found > $nth)
        {
            return substr_replace($subject, $replace, $matches[0][$nth][1], strlen($search));
        }

        return $subject;
    }

    /**
     * Supprime tout les tags vides d'un code html
     * @param string $html
     * @return string
     */
    public static function removeEmptyTags(string $html)
    {
        $pattern = '/<([^ >]+)[^>]*>(<br[^>]*>[[:space:]]*)*[[:space:]]*<\/\1>/ui';
        $html = str_replace( '&nbsp;', ' ', $html );

        do {
            $tmp = $html;
            $html = preg_replace(
                $pattern, '', $html );
        } while ( $html !== $tmp );

        return $html;
    }

    /**
     * Récupération du type unicode de la chaine de caractère
     * @param string $string
     * @return string
     */
    public function getUnicodeCharProperty(string $string)
    {
        if(preg_match('/[\p{Latin}]/u', $string))
        {
            return 'Latin';
        }
        if(preg_match('/[\p{Arabic}]/u', $string))
        {
            return 'Arabic';
        }
        if(preg_match('/[\p{Armenian}]/u', $string))
        {
            return 'Armenian';
        }
        if(preg_match('/[\p{Avestan}]/u', $string))
        {
            return 'Avestan';
        }
        if(preg_match('/[\p{Balinese}]/u', $string))
        {
            return 'Balinese';
        }
        if(preg_match('/[\p{Bamum}]/u', $string))
        {
            return 'Bamum';
        }
        if(preg_match('/[\p{Batak}]/u', $string))
        {
            return 'Batak';
        }
        if(preg_match('/[\p{Bengali}]/u', $string))
        {
            return 'Bengali';
        }
        if(preg_match('/[\p{Bopomofo}]/u', $string))
        {
            return 'Bopomofo';
        }
        if(preg_match('/[\p{Brahmi}]/u', $string))
        {
            return 'Brahmi';
        }
        if(preg_match('/[\p{Braille}]/u', $string))
        {
            return 'Braille';
        }
        if(preg_match('/[\p{Buginese}]/u', $string))
        {
            return 'Buginese';
        }
        if(preg_match('/[\p{Buhid}]/u', $string))
        {
            return 'Buhid';
        }
        if(preg_match('/[\p{Canadian_Aboriginal}]/u', $string))
        {
            return 'Canadian_Aboriginal';
        }
        if(preg_match('/[\p{Carian}]/u', $string))
        {
            return 'Carian';
        }
        if(preg_match('/[\p{Chakma}]/u', $string))
        {
            return 'Chakma';
        }
        if(preg_match('/[\p{Cham}]/u', $string))
        {
            return 'Cham';
        }
        if(preg_match('/[\p{Cherokee}]/u', $string))
        {
            return 'Cherokee';
        }
        if(preg_match('/[\p{Common}]/u', $string))
        {
            return 'Common';
        }
        if(preg_match('/[\p{Coptic}]/u', $string))
        {
            return 'Coptic';
        }
        if(preg_match('/[\p{Cuneiform}]/u', $string))
        {
            return 'Cuneiform';
        }
        if(preg_match('/[\p{Cypriot}]/u', $string))
        {
            return 'Cypriot';
        }
        if(preg_match('/[\p{Cyrillic}]/u', $string))
        {
            return 'Cyrillic';
        }
        if(preg_match('/[\p{Deseret}]/u', $string))
        {
            return 'Deseret';
        }
        if(preg_match('/[\p{Devanagari}]/u', $string))
        {
            return 'Devanagari';
        }
        if(preg_match('/[\p{Egyptian_Hieroglyphs}]/u', $string))
        {
            return 'Egyptian_Hieroglyphs';
        }
        if(preg_match('/[\p{Ethiopic}]/u', $string))
        {
            return 'Ethiopic';
        }
        if(preg_match('/[\p{Georgian}]/u', $string))
        {
            return 'Georgian';
        }
        if(preg_match('/[\p{Glagolitic}]/u', $string))
        {
            return 'Glagolitic';
        }
        if(preg_match('/[\p{Gothic}]/u', $string))
        {
            return 'Gothic';
        }
        if(preg_match('/[\p{Greek}]/u', $string))
        {
            return 'Greek';
        }
        if(preg_match('/[\p{Gujarati}]/u', $string))
        {
            return 'Gujarati';
        }
        if(preg_match('/[\p{Gurmukhi}]/u', $string))
        {
            return 'Gurmukhi';
        }
        if(preg_match('/[\p{Han}]/u', $string))
        {
            return 'Han';
        }
        if(preg_match('/[\p{Hangul}]/u', $string))
        {
            return 'Hangul';
        }
        if(preg_match('/[\p{Hanunoo}]/u', $string))
        {
            return 'Hanunoo';
        }
        if(preg_match('/[\p{Hebrew}]/u', $string))
        {
            return 'Hebrew';
        }
        if(preg_match('/[\p{Hiragana}]/u', $string))
        {
            return 'Hiragana';
        }
        if(preg_match('/[\p{Imperial_Aramaic}]/u', $string))
        {
            return 'Imperial_Aramaic';
        }
        if(preg_match('/[\p{Inherited}]/u', $string))
        {
            return 'Inherited';
        }
        if(preg_match('/[\p{Inscriptional_Pahlavi}]/u', $string))
        {
            return 'Inscriptional_Pahlavi';
        }
        if(preg_match('/[\p{Inscriptional_Parthian}]/u', $string))
        {
            return 'Inscriptional_Parthian';
        }
        if(preg_match('/[\p{Javanese}]/u', $string))
        {
            return 'Javanese';
        }
        if(preg_match('/[\p{Kaithi}]/u', $string))
        {
            return 'Kaithi';
        }
        if(preg_match('/[\p{Kannada}]/u', $string))
        {
            return 'Kannada';
        }
        if(preg_match('/[\p{Katakana}]/u', $string))
        {
            return 'Katakana';
        }
        if(preg_match('/[\p{Kayah_Li}]/u', $string))
        {
            return 'Kayah_Li';
        }
        if(preg_match('/[\p{Kharoshthi}]/u', $string))
        {
            return 'Kharoshthi';
        }
        if(preg_match('/[\p{Khmer}]/u', $string))
        {
            return 'Khmer';
        }
        if(preg_match('/[\p{Lao}]/u', $string))
        {
            return 'Lao';
        }
        if(preg_match('/[\p{Lepcha}]/u', $string))
        {
            return 'Lepcha';
        }
        if(preg_match('/[\p{Limbu}]/u', $string))
        {
            return 'Limbu';
        }
        if(preg_match('/[\p{Linear_B}]/u', $string))
        {
            return 'Linear_B';
        }
        if(preg_match('/[\p{Lisu}]/u', $string))
        {
            return 'Lisu';
        }
        if(preg_match('/[\p{Lycian}]/u', $string))
        {
            return 'Lycian';
        }
        if(preg_match('/[\p{Lydian}]/u', $string))
        {
            return 'Lydian';
        }
        if(preg_match('/[\p{Malayalam}]/u', $string))
        {
            return 'Malayalam';
        }
        if(preg_match('/[\p{Mandaic}]/u', $string))
        {
            return 'Mandaic';
        }
        if(preg_match('/[\p{Meetei_Mayek}]/u', $string))
        {
            return 'Meetei_Mayek';
        }
        if(preg_match('/[\p{Meroitic_Cursive}]/u', $string))
        {
            return 'Meroitic_Cursive';
        }
        if(preg_match('/[\p{Meroitic_Hieroglyphs}]/u', $string))
        {
            return 'Meroitic_Hieroglyphs';
        }
        if(preg_match('/[\p{Miao}]/u', $string))
        {
            return 'Miao';
        }
        if(preg_match('/[\p{Mongolian}]/u', $string))
        {
            return 'Mongolian';
        }
        if(preg_match('/[\p{Myanmar}]/u', $string))
        {
            return 'Myanmar';
        }
        if(preg_match('/[\p{New_Tai_Lue}]/u', $string))
        {
            return 'New_Tai_Lue';
        }
        if(preg_match('/[\p{Nko}]/u', $string))
        {
            return 'Nko';
        }
        if(preg_match('/[\p{Ogham}]/u', $string))
        {
            return 'Ogham';
        }
        if(preg_match('/[\p{Old_Italic}]/u', $string))
        {
            return 'Old_Italic';
        }
        if(preg_match('/[\p{Old_Persian}]/u', $string))
        {
            return 'Old_Persian';
        }
        if(preg_match('/[\p{Old_South_Arabian}]/u', $string))
        {
            return 'Old_South_Arabian';
        }
        if(preg_match('/[\p{Old_Turkic}]/u', $string))
        {
            return 'Old_Turkic';
        }
        if(preg_match('/[\p{Ol_Chiki}]/u', $string))
        {
            return 'Ol_Chiki';
        }
        if(preg_match('/[\p{Oriya}]/u', $string))
        {
            return 'Oriya';
        }
        if(preg_match('/[\p{Osmanya}]/u', $string))
        {
            return 'Osmanya';
        }
        if(preg_match('/[\p{Phags_Pa}]/u', $string))
        {
            return 'Phags_Pa';
        }
        if(preg_match('/[\p{Phoenician}]/u', $string))
        {
            return 'Phoenician';
        }
        if(preg_match('/[\p{Rejang}]/u', $string))
        {
            return 'Rejang';
        }
        if(preg_match('/[\p{Runic}]/u', $string))
        {
            return 'Runic';
        }
        if(preg_match('/[\p{Samaritan}]/u', $string))
        {
            return 'Samaritan';
        }
        if(preg_match('/[\p{Saurashtra}]/u', $string))
        {
            return 'Saurashtra';
        }
        if(preg_match('/[\p{Sharada}]/u', $string))
        {
            return 'Sharada';
        }
        if(preg_match('/[\p{Shavian}]/u', $string))
        {
            return 'Shavian';
        }
        if(preg_match('/[\p{Sinhala}]/u', $string))
        {
            return 'Sinhala';
        }
        if(preg_match('/[\p{Sora_Sompeng}]/u', $string))
        {
            return 'Sora_Sompeng';
        }
        if(preg_match('/[\p{Sundanese}]/u', $string))
        {
            return 'Sundanese';
        }
        if(preg_match('/[\p{Syloti_Nagri}]/u', $string))
        {
            return 'Syloti_Nagri';
        }
        if(preg_match('/[\p{Syriac}]/u', $string))
        {
            return 'Syriac';
        }
        if(preg_match('/[\p{Tagalog}]/u', $string))
        {
            return 'Tagalog';
        }
        if(preg_match('/[\p{Tagbanwa}]/u', $string))
        {
            return 'Tagbanwa';
        }
        if(preg_match('/[\p{Tai_Le}]/u', $string))
        {
            return 'Tai_Le';
        }
        if(preg_match('/[\p{Tai_Tham}]/u', $string))
        {
            return 'Tai_Tham';
        }
        if(preg_match('/[\p{Tai_Viet}]/u', $string))
        {
            return 'Tai_Viet';
        }
        if(preg_match('/[\p{Takri}]/u', $string))
        {
            return 'Takri';
        }
        if(preg_match('/[\p{Tamil}]/u', $string))
        {
            return 'Tamil';
        }
        if(preg_match('/[\p{Telugu}]/u', $string))
        {
            return 'Telugu';
        }
        if(preg_match('/[\p{Thaana}]/u', $string))
        {
            return 'Thaana';
        }
        if(preg_match('/[\p{Thai}]/u', $string))
        {
            return 'Thai';
        }
        if(preg_match('/[\p{Tibetan}]/u', $string))
        {
            return 'Tibetan';
        }
        if(preg_match('/[\p{Tifinagh}]/u', $string))
        {
            return 'Tifinagh';
        }
        if(preg_match('/[\p{Ugaritic}]/u', $string))
        {
            return 'Ugaritic';
        }
        if(preg_match('/[\p{Vai}]/u', $string))
        {
            return 'Vai';
        }
        if(preg_match('/[\p{Yi}]/u', $string))
        {
            return 'Yi';
        }
    }
}
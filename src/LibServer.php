<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions sur le serveur  
 */
class LibServer
{
    /**
     * Vérifie si le serveur est en https
     * @return bool
     */
    public static function is_secure() {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        }
        
        return $isSecure;
    }
    
    public static function get_protocole() {
        $isSecure = self::is_secure();
        if($isSecure == true) {
            return 'https://';
        }
        else {
            return 'http://';
        }
    }
    
    /**
     * Utilisation totale de la mémoire par le système
     * @return number
     */
    public static function getMemoryUsageTotal()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2] / $mem[1] * 100;
        
        return $memory_usage;
    }
    
    /**
     * Récupère la charge moyenne du CPU
     */
    public static function getSystemLoad($interval = 1)
    {
        $coreCount = self::getSystemCores();
        
        $rs = sys_getloadavg();
        $interval = $interval >= 1 && 3 <= $interval ? $interval : 1;
        $load = $rs[$interval];
        return round(($load * 100) / $coreCount, 2);
    }
    
    /**
     * Retourne le nombre de coeur du système
     * @return number
     */
    public static function getSystemCores()
    {
        $cmd = "uname";
        $OS = strtolower(trim(shell_exec($cmd)));
        
        switch($OS) {
            case('linux'):
                $cmd = "cat /proc/cpuinfo | grep processor | wc -l";
                break;
            case('freebsd'):
                $cmd = "sysctl -a | grep 'hw.ncpu' | cut -d ':' -f2";
                break;
            default:
                unset($cmd);
        }
        
        if ($cmd != '') {
            $cpuCoreNo = intval(trim(shell_exec($cmd)));
        }
        
        return empty($cpuCoreNo) ? 1 : $cpuCoreNo;
    }
    
    /**
     * Retourne le nombre de connexions HTTP
     * @return number
     */
    public static function getHttpConnections()
    {
        if (function_exists('exec'))
        {
            $www_total_count = 0;
            $results = array();
            @exec('netstat -an | egrep \':80|:443\' | awk \'{print $5}\' | grep -v \':::\*\' |  grep -v \'0.0.0.0\'', $results);
            
            $unique = array();
            $www_unique_count = 0;
            foreach ($results as $result) {
                $array = explode(':', $result);
                $www_total_count ++;
                
                if (preg_match('/^::/', $result)) {
                    $ipaddr = $array[3];
                } else {
                    $ipaddr = $array[0];
                }
                
                if (!in_array($ipaddr, $unique)) {
                    $unique[] = $ipaddr;
                    $www_unique_count ++;
                }
            }
            unset ($results);
            
            return count($unique);
        }
    }
    
    /**
     * Utilisation courante du disque
     * @return number
     */
    public static function getCurrentDiskUsage()
    {
        $disktotal = disk_total_space ('/');
        $diskfree  = disk_free_space  ('/');
        $diskuse   = round (100 - (($diskfree / $disktotal) * 100)) .'%';
        
        return $diskuse;
    }
    
    /**
     * Récupère l'uptime du serveur
     * @return number
     */
    public static function getServerUptime()
    {
        $uptime = floor(preg_replace ('/\.[0-9]+/', '', file_get_contents('/proc/uptime')) / 86400);
        
        return $uptime;
    }
    
    /**
     * Récupère la version du noyau
     * @return string
     */
    public static function getKernelVersion()
    {
        $kernel = explode(' ', file_get_contents('/proc/version'));
        $kernel = $kernel[2];
        
        return $kernel;
    }
    
    /**
     * Récupère le nombre de process en cours
     * @return number
     */
    public static function getNumberProcesses()
    {
        $proc_count = 0;
        $dh = opendir('/proc');
        
        while ($dir = readdir($dh)) {
            if (is_dir('/proc/' . $dir)) {
                if (preg_match('/^[0-9]+$/', $dir)) {
                    $proc_count ++;
                }
            }
        }
        
        return $proc_count;
    }
    
    /**
     * Utilisation de la mémoire en cours
     * @return string
     */
    public static function getCurrentMemoryUsage()
    {
        $mem = memory_get_usage(true);
        
        if ($mem < 1024) {
            $memory = $mem .' B';
        } elseif ($mem < 1048576) {
            $memory = round($mem / 1024, 2) .' KB';
        } else {
            $memory = round($mem / 1048576, 2) .' MB';
        }
        
        return $memory;
    }
    
    /**
     * Renvoi les informations sur la mémoire
     * @return array
     */
    public static function getSystemMemInfo()
    {
        $meminfo = @file_get_contents("/proc/meminfo");
        if ($meminfo) {
            $data = explode("\n", $meminfo);
            $meminfo = [];
            foreach ($data as $line) {
                if( strpos( $line, ':' ) !== false ) {
                    list($key, $val) = explode(":", $line);
                    $val = trim($val);
                    $val = preg_replace('/ kB$/', '', $val);
                    if (is_numeric($val)) {
                        $val = intval($val);
                    }
                    $meminfo[$key] = $val;
                }
            }
            return $meminfo;
        }
        
        return  null;
    }
    
    /**
     * Vérifie la mémoire
     * @return array
     */
    public static function checkMemory()
    {
        $memInfo = self::getSystemMemInfo();
        
        if ($memInfo) {
            $totalMemory = $memInfo['MemTotal'];
            $freeMemory = $memInfo['MemFree'];
            $swapTotalMemory = $memInfo['SwapTotal'];
            $swapFreeMemory = $memInfo['SwapFree'];
            if (($totalMemory / 100.0) * 30.0 > $freeMemory) {
                if (($swapTotalMemory / 100.0) * 50.0 > $swapFreeMemory) {
                    return array(
                        'result' => 0,
                        'message' => 'Less than 30% free memory and less than 50% free swap space'
                    );
                }
                return array(
                    'result' => 0,
                    'message' => 'Less than 30% free memory'
                );
            }
        }
        
        return array(
            'result' => 1,
            'message' => null
        );
    }
    
    /**
     * Récupération des informations du système
     */
    public static function getSystemInfos()
    {
        $stat = array();
        
        //cpu stat
        $prevVal = shell_exec("cat /proc/stat");
        $prevArr = explode(' ',trim($prevVal));
        $prevTotal = $prevArr[2] + $prevArr[3] + $prevArr[4] + $prevArr[5];
        $prevIdle = $prevArr[5];
        usleep(0.15 * 1000000);
        $val = shell_exec("cat /proc/stat");
        $arr = explode(' ', trim($val));
        $total = $arr[2] + $arr[3] + $arr[4] + $arr[5];
        $idle = $arr[5];
        $intervalTotal = intval($total - $prevTotal);
        $stat['cpu'] =  intval(100 * (($intervalTotal - ($idle - $prevIdle)) / $intervalTotal));
        $cpu_result = shell_exec("cat /proc/cpuinfo | grep model\ name");
        $stat['cpu_model'] = strstr($cpu_result, "\n", true);
        $stat['cpu_model'] = str_replace("model name    : ", "", $stat['cpu_model']);
        
        //memory stat
        $stat['mem_percent'] = round(shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'"), 2);
        $mem_result = shell_exec("cat /proc/meminfo | grep MemTotal");
        $stat['mem_total'] = round(preg_replace("#[^0-9]+(?:\.[0-9]*)?#", "", $mem_result) / 1024 / 1024, 3);
        $mem_result = shell_exec("cat /proc/meminfo | grep MemFree");
        $stat['mem_free'] = round(preg_replace("#[^0-9]+(?:\.[0-9]*)?#", "", $mem_result) / 1024 / 1024, 3);
        $stat['mem_used'] = $stat['mem_total'] - $stat['mem_free'];
        
        //hdd stat
        $stat['hdd_free'] = round(disk_free_space("/") / 1024 / 1024 / 1024, 2);
        $stat['hdd_total'] = round(disk_total_space("/") / 1024 / 1024/ 1024, 2);
        $stat['hdd_used'] = $stat['hdd_total'] - $stat['hdd_free'];
        $stat['hdd_percent'] = round(sprintf('%.2f',($stat['hdd_used'] / $stat['hdd_total']) * 100), 2);
        
        //network stat
        $stat['network_rx'] = round(trim(file_get_contents("/sys/class/net/eth0/statistics/rx_bytes")) / 1024/ 1024/ 1024, 2);
        $stat['network_tx'] = round(trim(file_get_contents("/sys/class/net/eth0/statistics/tx_bytes")) / 1024/ 1024/ 1024, 2);
        
        return $stat;
    }
}

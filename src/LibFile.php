<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions sur les fichiers
 */
class LibFile {

    /**
     * Récupération du nombre de lignes d'un fichier
     * @param string $path
     * @return int
     */
    public static function getNbLines($path)
    {
        if(!file_exists($path))
        {
            return false;
        }

        return count(file($path));
    }
}

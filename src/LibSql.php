<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions utilitaires sur les requêtes SQL   
 */
class LibSql {
    /**
     * Permet de construire une requête SQL à partir d'un tableau de données
     * 
     * @param string $tableName
     * @param array $data
     * @return string
     */
    public static function createInsertQuery($tableName, $data = array()) {
        if (empty($data)) {
            return null;
        }
        $fields = "";
        $values = "";
        $i = 1;
        foreach ($data as $field => $value) {
            $fields .= "`" . $field . "`";
            $fields .= ($i < count($data)) ? ", " : "";
            $values .= "'" . str_replace("'", "''", $value) . "'";
            $values .= ($i < count($data)) ? ", " : "";
            $i++;
        }
        return "INSERT INTO " . $tableName . " (" . $fields . ") VALUES (" . $values . ");\n";
    }

    /**
     * Construction de requête UNION à partir de QueryBuilder de doctrine
     * @param string $order
     * @param QueryBuilder ...$queries
     * @return string
     */
    public static function doctrineUnion($order, \Doctrine\ORM\QueryBuilder ...$queries){
        $sqlList = array();
        foreach ($queries as $query) {
            $sql = $query->getQuery()->getSql();
            foreach($query->getQuery()->getParameters() as $parameter)
            {
                $pos = strpos($sql, '?');
                if($pos !== false)
                {
                    if(is_bool($parameter->getValue()))
                    {
                        if($parameter->getValue() == true)
                        {
                            $value = 1;
                        }
                        else
                        {
                            $value = 0;
                        }
                    }
                    else
                    {
                        $value = $parameter->getValue();
                    }
                    $sql = substr_replace($sql, $value, $pos, strlen('?'));
                }
            }
            $sqlList[] = '('.$sql.')';
        }

        $sql = implode(' UNION ', $sqlList);

        if(!empty($order))
        {
            $sql = 'SELECT * FROM ('.$sql.') AS q ORDER BY '.$order;
        }

        return $sql;
    }
}

<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions diverses  
 */
class LibTools
{

    /**
     * Renvoi l'architecture complète d'un répertoire
     * @param string $path
     * @return array
     */
    public static function getDirectoryArchi($path)
    {
        $openDir = opendir($path);

        $archi = array();
        while (($entry = readdir($openDir)) !== false)
        {
            if (is_dir($path . DIRECTORY_SEPARATOR . $entry) && $entry != '.' && $entry != '..')
            {
                $archi[$entry] = self::getArchi($path . DIRECTORY_SEPARATOR . $entry);
            }
            elseif (($entry != '.') && ($entry != '..'))
            {
                $archi[] = $entry;
            }
        }
        closedir($openDir);

        return $archi;
    }

    /**
     * Converts bytes into human readable file size.
     * @param string $bytes 
     * @return string human readable file size
     */
    public static function fileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach ($arBytes as $arItem)
        {
            if ($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
    
    /**
     * Return a unique code on 10 characters
     * @return string
     */
    public static function generateUniqCode()
    {
        $code = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10));
        $code = strtr($code, '0O', 'AA');
        return $code;
    }
    
    /**
     * Génération d'un token
     * @param int $length
     * @return string
     */
    public static function createAuthToken($length = 64)
    {
        return bin2hex(random_bytes($length));
    }
}

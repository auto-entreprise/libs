<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions sur la localisation
 */
class LibLocation {
    /**
     * Récupération des infos du pays de l'utilisateur
     * @param string $ip
     * @return array
     */
    public static function getLocationInfoByIp($ip = null)
    {
        if(empty($ip))
        {
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = @$_SERVER['REMOTE_ADDR'];
            if(filter_var($client, FILTER_VALIDATE_IP))
            {
                $ip = $client;
            }
            elseif(filter_var($forward, FILTER_VALIDATE_IP))
            {
                $ip = $forward;
            }
            else
            {
                $ip = $remote;
            }
        }

        $url = 'http://www.geoplugin.net/json.gp?ip='.$ip;
        $ipData = @json_decode(file_get_contents($url));

        return $ipData;
    }
}

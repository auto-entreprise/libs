<?php

namespace Jherrera\Libs;

/**
 * Classe offrant un pannel de fonctions sur les captchas google
 */
class LibCaptcha
{
    const SERVER = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Vérification du token
     * @param string $token
     * @param string $key
     * @return mixed
     */
    public static function checkToken($token, $key)
    {
        $url = self::SERVER;
        $data = array('secret' => $key, 'response' => $token);

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);

        return json_decode($response,true);
    } 
}